import cv2
import numpy as np

#Se utiliza el cascade para reconocer los rostros
faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_frontalface_default.xml')

image = cv2.imread('oficina.png')
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

#Todos estos son atributos que se aplican a las imagenes, hay mas en la documentacion de OPENCV
faces = faceClassif.detectMultiScale(gray,
	scaleFactor=1.1,  #--> Parametro para saber que tanto se reduce la imagen
	minNeighbors=5,  #--> Detecta todos los rostros que se identifican
	minSize=(30,30),  #--> Tamaño minimo posible de objeto
	maxSize=(200,200)) #-->Tamaño maximo posible de objeto (mientras mas grande sea , mas sera ignorado)

#Los puntos x - y son los puntos en caso que se detecte una imagen
#donde se guardan el alto y ancho del rostro o imagen y se guardan en face
#luego se le aplica un triangulo y se guardan 
for (x,y,w,h) in faces:
	cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),2)

cv2.imshow('image',image)
cv2.waitKey(0)
cv2.destroyAllWindows()
