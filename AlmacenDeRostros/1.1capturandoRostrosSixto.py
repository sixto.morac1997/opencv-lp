import cv2

faceClassif = cv2.CascadeClassifier(cv2.data.haarcascades+'haarcascade_frontalface_default.xml')

image = cv2.imread('frailes.jpeg')
imageAux = image.copy()
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

faces = faceClassif.detectMultiScale(gray, 1.1, 5)

count = 0

for (x,y,w,h) in faces:

	cv2.rectangle(image, (x,y),(x+w,y+h),(128,0,255),2)
	rostro = image[y:y+h,x:x+w]   # Si solo pones image sale con el cuadrado morado, si usas la variable imageAux saldra la imagen con el recorte neto. 
	rostro = cv2.resize(rostro,(150,150), interpolation=cv2.INTER_CUBIC)  # Si omitimos esta linea de codigo, se guarda la imagen con un formato diferenciado al que se calcula en el recuadro. 
	cv2.imwrite('rostro_{}.jpg'.format(count),rostro)
	count = count + 1

	cv2.imshow('rostro',rostro)
	cv2.imshow('image',image)
	cv2.waitKey(0)

cv2.destroyAllWindows()
